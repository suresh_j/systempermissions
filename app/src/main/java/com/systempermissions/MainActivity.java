package com.systempermissions;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {
    private Dialog dialog;
    private ArrayList<String> selectedImagesFilePaths;
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selectedImagesFilePaths = new ArrayList<>();

        openSelectImageDialog();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_TAKE_PICTURE || requestCode == REQUEST_PICK_PHOTO || requestCode == REQUEST_IMAGE_CHOOSER) {
            if (data != null && data.getData() != null) {
                filePath = Utils.getFilePath(this, data.getData());
            } else {
                filePath = getCurrentPhotoPath();
            }

            if (filePath != null) {

            } else {
                showToast("Error uploading image, Unable locate image..!");
            }
        } else if (requestCode == REQUEST_VIDEO_CHOOSER) {
            //upload video code
        }

    }

    private void openSelectImageDialog() {
        dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setTitle("");
        dialog.setContentView(R.layout.dilog_cam_gal);
        dialog.setCancelable(true);
        (dialog.findViewById(R.id.empty_ll)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedImagesFilePaths.size() > 0) {
                    dialog.dismiss();
                } else {
                    finish();
                }
            }
        });
        (dialog.findViewById(R.id.video_tv)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchVideoChooserIntent();
            }
        });
        (dialog.findViewById(R.id.image_tv)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchImageChooserIntent();
            }
        });
        //check permissions
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        //in permission is not granted ask user
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "Permission not granted, granting now...");
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(this, PERMISSIONS_VIDEO_CHOOSER, 1);
        } else {
            dialog.show();
        }
    }


}
